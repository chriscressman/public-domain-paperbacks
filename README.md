I presented "Architecting a Search Feature (in Rails)" to the
Lancaster Elastic user group on August 28, 2019.

This repo contains the presentation slides and the Ruby on Rails
application I wrote for the presentation and pulled the example
code from.

For the slides, see presentation.odp or presentation.pdf.

-- Chris Cressman
